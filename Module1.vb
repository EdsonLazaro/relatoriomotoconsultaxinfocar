﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Math
Imports System.Data
Imports System.Text.RegularExpressions
Imports MySql.Data.MySqlClient
Module Module1

    Dim caminho1 As String = "C:\salvarDiferenca" '"C:\LOGXMLS\"
    Dim local As String = "C:\salvarDiferenca\detalhado\" ' & Date.Now.ToString("ddMMyyyy") & "\"
    Sub Main()
        Dim i As String = 0

        '   For Each arquivos As String In Directory.GetDirectories(caminho1)

        '  For Each local2 As String In Directory.GetDirectories(caminho1)


        '  For Each endereco As String In Directory.GetFiles(local2)


        Dim lista As New List(Of String)
        For Each arq As String In Directory.GetFiles(caminho1)
            Dim arquivo As New FileInfo(arq)
            If Regex.IsMatch(arquivo.Name.ToLower, "\.(csv|pdf|txt|dig|htm|html|xml)$") Then
                Dim texto As String = ""
                Dim reader As New StreamReader(arquivo.FullName, System.Text.ASCIIEncoding.Default)
                '  texto = limpaHtml(reader.ReadToEnd)
                texto = reader.ReadToEnd
                reader.Close()

                If Not String.IsNullOrEmpty(texto) Then
                    Dim vetor() As String = Regex.Split(texto, "[\r\n]+")
                    ' DESCRICAO,DATAHORA,PLACACHASSI,DOCUMENTO,NREGISTRO
                    For Each pegavetor In vetor
                        Dim vetor2() As String = Regex.Split(pegavetor, ",")
                        Dim DESCRICAO As String = vetor2(0).Replace("'", "")
                        Dim DATAHORA As String = vetor2(1).Replace("'", "")
                        Dim PLACACHASSI As String = vetor2(2).Replace("'", "")
                        Dim DOCUMENTO As String = vetor2(3).Replace("'", "")
                        Dim NREGISTRO As String = vetor2(4).Replace("'", "")
                        '  If Not CONFEREBanco(DESCRICAO, DATAHORA, PLACACHASSI, DOCUMENTO) Then
                        Dim query As String = pegavetor
                        '  INSERTBanco(query)
                        Dim Mesano As String = Regex.Match(DATAHORA, "/\d{2}/\d{4}").Value.Replace("/", "")
                        If Regex.IsMatch(DESCRICAO, "Veicular", RegexOptions.IgnoreCase) And Not Regex.IsMatch(DESCRICAO, "360", RegexOptions.IgnoreCase) Then
                            salva(query, DESCRICAO, Mesano)

                        ElseIf Regex.IsMatch(DESCRICAO, "Renajud", RegexOptions.IgnoreCase) Then
                            salva(query, DESCRICAO, Mesano)
                        ElseIf Regex.IsMatch(DESCRICAO, "CNH", RegexOptions.IgnoreCase) Then
                            salva(query, DESCRICAO, Mesano)
                        ElseIf Regex.IsMatch(DESCRICAO, "Agregados", RegexOptions.IgnoreCase) Then
                            salva(query, DESCRICAO, Mesano)
                        ElseIf Regex.IsMatch(DESCRICAO, "Veicular 360", RegexOptions.IgnoreCase) Then
                            salva(query, DESCRICAO, Mesano)
                        End If
                        Threading.Thread.Sleep(10)
                        'Next
                    Next


                End If
            End If
            'Next
        Next

        '    Next


    End Sub



    Public Function limpaHtml(ByVal texto As String) As String
        Return Regex.Replace(Regex.Replace(Regex.Replace(texto, "<[^>]*>|&[^;]+;", " "), "[\r\n\t]", " "), "[\s]{2,}", " ").Trim
    End Function

    Function INSERTBanco(ByVal query As String) As Boolean
        '`basepropeietario`.`dados` 
        Dim conexao As New MySqlConnection("Server=Localhost;user id=root;password=m3p1g5a8s9;database=basepropeietario")

        If conexao.State = ConnectionState.Closed Then
            conexao.Open()
        End If

        Dim command As New MySqlCommand(query, conexao)
        Dim reader = command.ExecuteReader
        reader.Close()

        If conexao.State = ConnectionState.Open Then
            conexao.Close()
        End If
        Return False
    End Function

    Function CONFEREBanco(ByVal descricao As String, ByVal DATAHORA As String, ByVal PLACACHASSI As String, ByVal documento As String) As Boolean
        '`basepropeietario`.`dados` 
        Dim conexao As New MySqlConnection("Server=Localhost;user id=root;password=m3p1g5a8s9;database=pesquisas")
        ' descricao = PLACACHASSI.Replace("'", "").Trim
        Dim ano As String = Regex.Match(DATAHORA, "/\d{4}").Value.Replace("/", "")  'CNH
        documento = documento.Replace("'", "").Trim
        If conexao.State = ConnectionState.Closed Then
            conexao.Open()
        End If
        Dim existe As Boolean = False
        Dim query As String = String.Empty
        If Regex.IsMatch(descricao, "Veicular", RegexOptions.IgnoreCase) And Not Regex.IsMatch(descricao, "360", RegexOptions.IgnoreCase) Then
            query = "select * from `pesquisas`.`log_pesquisa_bn` where fornecedor = 'MOTOCONSULTA' and dado= '" & PLACACHASSI & "' and horario like '%" & ano & "%' "
            Dim command As New MySqlCommand(query, conexao)
            Dim reader = command.ExecuteReader
            existe = reader.Read
            reader.Close()
        ElseIf Regex.IsMatch(descricao, "Renajud", RegexOptions.IgnoreCase) Then
            query = "select * from `pesquisas`.`log_pesquisa_bn` where fornecedor = 'MOTOCONSULTA-RENAJUD' and dado= '" & PLACACHASSI & "' and horario like '%" & ano & "%' "
            Dim command As New MySqlCommand(query, conexao)
            Dim reader = command.ExecuteReader
            existe = reader.Read
            reader.Close()
        ElseIf Regex.IsMatch(descricao, "CNH", RegexOptions.IgnoreCase) Then
            query = "select * from `pesquisas`.`log_pesquisa_bn` where fornecedor = 'MOTORCONSULTA-CNH' and dado= '" & documento & "' and horario like '%" & ano & "%' "
            Dim command As New MySqlCommand(query, conexao)
            Dim reader = command.ExecuteReader
            existe = reader.Read
            reader.Close()
        ElseIf Regex.IsMatch(descricao, "Agregados", RegexOptions.IgnoreCase) Then
            query = "select * from `pesquisas`.`log_pesquisa_bn` where fornecedor = 'MOTORCONSULTA-AGREGADOS' and dado= '" & PLACACHASSI & "' and horario like '%" & ano & "%' "
            Dim command As New MySqlCommand(query, conexao)
            Dim reader = command.ExecuteReader
            existe = reader.Read
            reader.Close()
        ElseIf Regex.IsMatch(descricao, "Veicular 360", RegexOptions.IgnoreCase) Then
            query = "select * from `pesquisas`.`log_pesquisa_bn` where fornecedor = 'MOTORCONSULTA-360' and dado= '" & PLACACHASSI & "' and horario like '%" & ano & "%' "
            Dim command As New MySqlCommand(query, conexao)
            Dim reader = command.ExecuteReader
            existe = reader.Read
            reader.Close()
        End If

        '  Return existe
        If conexao.State = ConnectionState.Open Then
            conexao.Close()
        End If
        Return existe
    End Function

    Public Sub salva(ByVal arquivo As String, ByVal pesquisa As String, ByVal anomes As String)
        If Not IO.Directory.Exists(local) Then
            IO.Directory.CreateDirectory(local)
        End If
        Dim cmDados As String = local & "naotem_arquivo_ " & pesquisa & "_" & anomes & "" & ".txt"
        Dim stream As New IO.StreamWriter(cmDados, IO.FileMode.Append)
        stream.WriteLine(arquivo)
        stream.Flush()
        stream.Close()
    End Sub
End Module
